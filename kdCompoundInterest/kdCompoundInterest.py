'''
Created on 2019年5月10日

@author: bkd
'''
import sys

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QMainWindow

from .kdCompoundInterest_ui import Ui_MainWindow


class kdCompoundInterest(QMainWindow, Ui_MainWindow):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
#         self.setWindowIcon(
#             QIcon(get_file_realpath("logo.jpg")))

    @pyqtSlot()
    def on_pb_calc_clicked(self):
        month = self.sb_month.value()
        month_deposit = self.sb_deposit_monthly.value()
        interest_rate = self.dbsb_interest_rate.value()
        print(month)
        str_result = ""
        total_capital = 0
        for i in range(month):
            total_capital = total_capital * \
                (1 + interest_rate)
            total_capital += month_deposit
            str_result = str_result + \
                "第" + str(i + 1) + "个月,存款：" + \
                str(int(total_capital)) + "，利息：" + \
                str(int(total_capital * interest_rate)) + "\n"
        print(str_result)
        self.tb_result.setText(str_result)


def main():
    app = QApplication(sys.argv)
    win = kdCompoundInterest()
    win.show()
    sys.exit(app.exec_())
