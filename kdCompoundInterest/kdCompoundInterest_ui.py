# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/bkd/pyqt/kdCompoundInterest/kdCompoundInterest/kdCompoundInterest.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(273, 426)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.sb_capital = QtWidgets.QSpinBox(self.centralwidget)
        self.sb_capital.setMaximum(999999999)
        self.sb_capital.setProperty("value", 10000)
        self.sb_capital.setObjectName("sb_capital")
        self.gridLayout.addWidget(self.sb_capital, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.sb_month = QtWidgets.QSpinBox(self.centralwidget)
        self.sb_month.setMaximum(99999)
        self.sb_month.setProperty("value", 24)
        self.sb_month.setObjectName("sb_month")
        self.gridLayout.addWidget(self.sb_month, 2, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.sb_deposit_monthly = QtWidgets.QSpinBox(self.centralwidget)
        self.sb_deposit_monthly.setMaximum(999999)
        self.sb_deposit_monthly.setProperty("value", 10000)
        self.sb_deposit_monthly.setObjectName("sb_deposit_monthly")
        self.gridLayout.addWidget(self.sb_deposit_monthly, 3, 1, 1, 1)
        self.pb_calc = QtWidgets.QPushButton(self.centralwidget)
        self.pb_calc.setObjectName("pb_calc")
        self.gridLayout.addWidget(self.pb_calc, 4, 1, 1, 1)
        self.tb_result = QtWidgets.QTextBrowser(self.centralwidget)
        self.tb_result.setObjectName("tb_result")
        self.gridLayout.addWidget(self.tb_result, 5, 0, 1, 2)
        self.dbsb_interest_rate = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.dbsb_interest_rate.setDecimals(3)
        self.dbsb_interest_rate.setSingleStep(0.001)
        self.dbsb_interest_rate.setProperty("value", 0.025)
        self.dbsb_interest_rate.setObjectName("dbsb_interest_rate")
        self.gridLayout.addWidget(self.dbsb_interest_rate, 1, 1, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 273, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "复利计算器"))
        self.label.setText(_translate("MainWindow", "本金"))
        self.label_2.setText(_translate("MainWindow", "利率"))
        self.label_3.setText(_translate("MainWindow", "月数"))
        self.label_4.setText(_translate("MainWindow", "每月定存"))
        self.pb_calc.setText(_translate("MainWindow", "计算"))


